<?php

namespace Samy\OAuth2;

/**
 * Describes OAuth2 Request interface.
 */
interface RequestInterface
{
    /**
     * Retrieve provided grant type.
     *
     * @return string
     */
    public function getGrantType(): string;

    /**
     * Return an instance with provided grant type.
     *
     * @param[in] string $GrantType Grant type
     *
     * @return static
     */
    public function withGrantType(string $GrantType): self;


    /**
     * Retrieve provided client id.
     *
     * @return string
     */
    public function getClientId(): string;

    /**
     * Return an instance with provided client id.
     *
     * @param[in] string $ClientId Client id
     *
     * @return static
     */
    public function withClientId(string $ClientId): self;


    /**
     * Retrieve provided client secret.
     *
     * @return string
     */
    public function getClientSecret(): string;

    /**
     * Return an instance with provided client secret.
     *
     * @param[in] string $ClientSecret Client secret
     *
     * @return static
     */
    public function withClientSecret(string $ClientSecret): self;


    /**
     * Retrieve provided username.
     *
     * @return string
     */
    public function getUsername(): string;

    /**
     * Return an instance with provided username.
     *
     * @param[in] string $Username Username
     *
     * @return static
     */
    public function withUsername(string $Username): self;


    /**
     * Retrieve provided password.
     *
     * @return string
     */
    public function getPassword(): string;

    /**
     * Return an instance with provided password.
     *
     * @param[in] string $Password Password
     *
     * @return static
     */
    public function withPassword(string $Password): self;


    /**
     * Retrieve provided scope.
     *
     * @return string
     */
    public function getScope(): string;

    /**
     * Return an instance with provided scope.
     *
     * @param[in] string $Scope Scope
     *
     * @return static
     */
    public function withScope(string $Scope): self;
}
