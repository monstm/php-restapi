<?php

namespace Samy\OAuth2;

use Psr\Http\Message\ResponseInterface;
use Samy\Psr18\Client;
use Samy\Psr7\Request;
use Samy\Psr7\Stream;
use Samy\Psr7\Uri;

/**
 * Simple OAuth2 ClientRequest implementation.
 */
class ClientRequest extends Request implements RequestInterface
{
    use RequestTrait;


    /**
     * Send request action.
     *
     * @param[in] string $EndPoint End point url
     *
     * @return Response
     */
    public function sendRequest(string $EndPoint): Response
    {
        $this->renderRequest($EndPoint);

        $client = new Client();
        $response = $client->sendRequest($this);

        return $this->renderResponse($response);
    }


    /**
     * Render request.
     *
     * @param[in] string $EndPoint Request end point
     *
     * @return void
     */
    private function renderRequest(string $EndPoint): void
    {
        $request = array();
        $maps = array(
            "grant_type" => $this->grant_type,
            "client_id" => $this->client_id,
            "client_secret" => $this->client_secret,
            "username" => $this->username,
            "password" => $this->password,
            "scope" => $this->scope
        );

        foreach ($maps as $key => $value) {
            if ($value != "") {
                $request[$key] = $value;
            }
        }

        $content = http_build_query($request);

        $stream = new Stream();
        $stream->withTemp();
        $stream->write($content);
        $stream->rewind();

        $uri = new Uri();
        $uri->parseUrl($EndPoint);

        $this
            ->withMethod("POST")
            ->withHeader("Accept", "application/json")
            ->withHeader("Content-Type", "application/x-www-form-urlencoded")
            ->withHeader("Content-Length", strlen($content))
            ->withBody($stream)
            ->withUri($uri);
    }

    /**
     * Render response.
     *
     * @param[in] ResponseInterface $Response OAuth2 response.
     *
     * @return Response
     */
    private function renderResponse(ResponseInterface $Response): Response
    {
        $ret = new Response();

        return $ret->parse($Response);
    }
}
