<?php

namespace Samy\OAuth2;

/**
 * Describes OAuth2 Request reuse code.
 */
trait RequestTrait
{
    /** describe grant type */
    protected $grant_type = "";

    /** describe client id */
    protected $client_id = "";

    /** describe client secret */
    protected $client_secret = "";

    /** describe username */
    protected $username = "";

    /** describe password */
    protected $password = "";

    /** describe scope */
    protected $scope = "";


    /**
     * Retrieve provided grant type.
     *
     * @return string
     */
    public function getGrantType(): string
    {
        return $this->grant_type;
    }

    /**
     * Retrieve provided grant type.
     *
     * @param[in] string $GrantType Grant type
     *
     * @return static
     */
    public function withGrantType(string $GrantType): self
    {
        $this->grant_type = $GrantType;

        return $this;
    }

    /**
     * Retrieve provided client id.
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->client_id;
    }

    /**
     * Return an instance with provided client id.
     *
     * @param[in] string $ClientId Client id
     *
     * @return static
     */
    public function withClientId(string $ClientId): self
    {
        $this->client_id = $ClientId;

        return $this;
    }

    /**
     * Retrieve provided client secret.
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    /**
     * Return an instance with provided client secret.
     *
     * @param[in] string $ClientSecret Client secret
     *
     * @return static
     */
    public function withClientSecret(string $ClientSecret): self
    {
        $this->client_secret = $ClientSecret;

        return $this;
    }

    /**
     * Retrieve provided username.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Return an instance with provided username.
     *
     * @param[in] string $Username Username
     *
     * @return static
     */
    public function withUsername(string $Username): self
    {
        $this->username = $Username;

        return $this;
    }

    /**
     * Retrieve provided password.
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Return an instance with provided password.
     *
     * @param[in] string $Password Password
     *
     * @return static
     */
    public function withPassword(string $Password): self
    {
        $this->password = $Password;

        return $this;
    }

    /**
     * Retrieve provided scope.
     *
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * Return an instance with provided scope.
     *
     * @param[in] string $Scope Scope
     *
     * @return static
     */
    public function withScope(string $Scope): self
    {
        $this->scope = $Scope;

        return $this;
    }
}
