<?php

namespace Samy\OAuth2;

/**
 * Describes OAuth2 Response interface.
 *
 * @see https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/
 */
interface ResponseInterface
{
    /**
     * Retrieve provided token type.
     *
     * @return string
     */
    public function getTokenType(): string;

    /**
     * Return an instance with provided token type.
     *
     * @param[in] string $TokenType Token type
     *
     * @return static
     */
    public function withTokenType(string $TokenType): self;


    /**
     * Retrieve provided access token.
     *
     * @return string
     */
    public function getAccessToken(): string;

    /**
     * Return an instance with provided access token.
     *
     * @param[in] string $AccessToken Access token
     *
     * @return static
     */
    public function withAccessToken(string $AccessToken): self;


    /**
     * Retrieve provided refresh token.
     *
     * @return string
     */
    public function getRefreshToken(): string;

    /**
     * Return an instance with provided refresh token.
     *
     * @param[in] string $RefreshToken Refresh token
     *
     * @return static
     */
    public function withRefreshToken(string $RefreshToken): self;


    /**
     * Retrieve provided token duration.
     *
     * @return int
     */
    public function getExpiresIn(): int;

    /**
     * Return an instance with provided token duration.
     *
     * @param[in] int $ExpiresIn Token duration
     *
     * @return static
     */
    public function withExpiresIn(int $ExpiresIn): self;


    /**
     * Retrieve provided scope.
     *
     * @return string
     */
    public function getScope(): string;

    /**
     * Return an instance with provided scope.
     *
     * @param[in] string $Scope Scope
     *
     * @return static
     */
    public function withScope(string $Scope): self;


    /**
     * Retrieve provided error parameter
     *
     * @return string
     */
    public function getError(): string;

    /**
     * Return an instance with error parameter.
     *
     * @param[in] string $Error Error parameter.
     *
     * @return static
     */
    public function withError(string $Error): self;


    /**
     * Retrieve provided error description.
     *
     * @return string
     */
    public function getErrorDescription(): string;

    /**
     * Return an instance with error description.
     *
     * @param[in] string $ErrorDescription Error description.
     *
     * @return static
     */
    public function withErrorDescription(string $ErrorDescription): self;


    /**
     * Retrieve provided error uri.
     *
     * @return string
     */
    public function getErrorUri(): string;

    /**
     * Return an instance with error uri.
     *
     * @param[in] string $ErrorUri Error uri.
     *
     * @return static
     */
    public function withErrorUri(string $ErrorUri): self;
}
