<?php

namespace Samy\OAuth2;

use Samy\Psr7\ServerRequest as Psr7ServerRequest;

/**
 * Simple OAuth2 ServerRequest implementation.
 */
class ServerRequest extends Psr7ServerRequest implements RequestInterface
{
    use RequestTrait;


    /**
     * OAuth2 ServerRequest construction.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $data = $this->getParsedBody();

        $this->grant_type = ($data["grant_type"] ?? "");

        if ($this->getAttribute("has_authorization", false)) {
            $this->client_id = $this->getAttribute("auth_username", "");
            $this->client_secret = $this->getAttribute("auth_password", "");
        } else {
            $this->client_id = ($data["client_id"] ?? "");
            $this->client_secret = ($data["client_secret"] ?? "");
        }

        $this->username = ($data["username"] ?? "");
        $this->password = ($data["password"] ?? "");

        $this->withScope($data["scope"] ?? "");
    }
}
