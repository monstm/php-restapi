<?php

namespace Samy\OAuth2;

use Samy\Log\Syslog;
use Samy\Psr7\Stream;
use Samy\RestApi\AbstractResponse;
use Samy\RestApi\AuthType;
use Samy\RestApi\ErrorResponseTrait;

/**
 * Simple OAuth2 Response implementation.
 */
class Response extends AbstractResponse implements ResponseInterface
{
    use ErrorResponseTrait;


    /** describe token type */
    protected $token_type = AuthType::BEARER;

    /** describe access token */
    protected $access_token = "";

    /** describe refresh token */
    protected $refresh_token = "";

    /** describe token duration */
    protected $expires_in = 0;

    /** describe scope */
    protected $scope = "";


    /**
     * Retrieve provided token type.
     *
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->token_type;
    }

    /**
     * Return an instance with provided token type.
     *
     * @param[in] string $TokenType Token type
     *
     * @return static
     */
    public function withTokenType(string $TokenType): self
    {
        $this->token_type = $TokenType;

        return $this;
    }


    /**
     * Retrieve provided access token.
     *
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * Return an instance with provided access token.
     *
     * @param[in] string $AccessToken Access token
     *
     * @return static
     */
    public function withAccessToken(string $AccessToken): self
    {
        $this->access_token = $AccessToken;

        return $this;
    }


    /**
     * Retrieve provided refresh token.
     *
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refresh_token;
    }

    /**
     * Return an instance with provided refresh token.
     *
     * @param[in] string $RefreshToken Refresh token
     *
     * @return static
     */
    public function withRefreshToken(string $RefreshToken): self
    {
        $this->refresh_token = $RefreshToken;

        return $this;
    }


    /**
     * Retrieve provided token duration.
     *
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expires_in;
    }

    /**
     * Return an instance with provided token duration.
     *
     * @param[in] int $ExpiresIn Token duration
     *
     * @return static
     */
    public function withExpiresIn(int $ExpiresIn): self
    {
        $this->expires_in = $ExpiresIn;

        return $this;
    }


    /**
     * Retrieve provided scope.
     *
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * Return an instance with provided scope.
     *
     * @param[in] string $Scope Scope
     *
     * @return static
     */
    public function withScope(string $Scope): self
    {
        $this->scope = $Scope;

        return $this;
    }


    /**
     * Overwrite Psr7 withBody.
     *
     * @param[in] mixed $StreamInterface Body.
     *
     * @return static
     */
    public function withBody($StreamInterface): self
    {
        $log = new Syslog();

        $StreamInterface->rewind();
        $content = $StreamInterface->getContents();
        $StreamInterface->rewind();


        $json = @json_decode($content, true);

        if (is_array($json)) {
            $this->withTokenType(
                isset($json["token_type"]) &&
                    is_string($json["token_type"]) ?
                    $json["token_type"] : ""
            );

            $this->withAccessToken(
                isset($json["access_token"]) &&
                    is_string($json["access_token"]) ?
                    $json["access_token"] : ""
            );

            $this->withRefreshToken(
                isset($json["refresh_token"]) &&
                    is_string($json["refresh_token"]) ?
                    $json["refresh_token"] : ""
            );

            $this->withExpiresIn(
                isset($json["expires_in"]) &&
                    is_int($json["expires_in"]) ?
                    $json["expires_in"] : 0
            );

            $this->withScope(
                isset($json["scope"]) &&
                    is_string($json["scope"]) ?
                    $json["scope"] : ""
            );

            $this->parseErrorResponse($json);
        } else {
            $log->backtrace(json_last_error_msg());
        }

        return $this;
    }

    /**
     * Overwrite Psr7 getBody.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody()
    {
        $json = array();
        $status = $this->getStatusCode();

        if (($status >= 200) && ($status < 300)) {
            $json["token_type"] = $this->token_type;

            $json["access_token"] = $this->access_token;

            if ($this->refresh_token != "") {
                $json["refresh_token"] = $this->refresh_token;
            }

            if ($this->expires_in > 0) {
                $json["expires_in"] = $this->expires_in;
            }

            if ($this->scope != "") {
                $json["scope"] = $this->scope;
            }
        } else {
            $this->raiseErrorResponse($json);
        }

        $content = json_encode($json);


        $stream = new Stream();

        $stream->withTemp();

        if (is_string($content)) {
            $this
                ->withHeader("Content-Type", "application/json")
                ->withHeader("Content-Length", strlen($content));

            $stream->write($content);
            $stream->rewind();
        }

        return $stream;
    }
}
