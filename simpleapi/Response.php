<?php

namespace Samy\SimpleApi;

use Samy\Log\Syslog;
use Samy\Psr7\Stream;
use Samy\RestApi\AbstractResponse;
use Samy\RestApi\ErrorResponseTrait;

/**
 * Simple SimpleApi Response implementation.
 */
class Response extends AbstractResponse implements ResponseInterface
{
    use ErrorResponseTrait;


    /** describe success status */
    protected $success = false;

    /** describe data */
    protected $data = null;

    /** describe messages */
    protected $message = array();

    /** describe links */
    protected $link = array();


    /**
     * Retrieve provided success status.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * Return an instance with provided success status.
     *
     * @param[in] bool $Success Success status
     *
     * @return static
     */
    public function withSuccess(bool $Success): self
    {
        $this->success = $Success;

        return $this;
    }


    /**
     * Retrieve provided data.
     *
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    /**
     * Return an instance with provided data.
     *
     * @param[in] mixed $Data Data
     *
     * @return static
     */
    public function withData(mixed $Data): self
    {
        $this->data = $Data;

        return $this;
    }


    /**
     * Retrieve all provided messages.
     *
     * @return array<array<string, mixed>>
     */
    public function getMessages(): array
    {
        return $this->message;
    }

    /**
     * Return an instance with added message.
     *
     * @param[in] string $Type Message type
     * @param[in] string $Text Message text
     * @param[in] mixed $Data Message data
     *
     * @return static
     */
    public function addMessage(string $Type, string $Text, mixed $Data = null): self
    {
        array_push($this->message, array(
            "type" => $Type,
            "text" => $Text,
            "data" => $Data
        ));

        return $this;
    }

    /**
     * Return an instance without any messages.
     *
     * @return static
     */
    public function cleanMessages(): self
    {
        $this->message = array();

        return $this;
    }


    /**
     * Checks if a link exists by the given case-insensitive name.
     *
     * @param[in] string $Name Link name
     *
     * @return bool
     */
    public function hasLink(string $Name): bool
    {
        $key = strtolower(trim($Name));

        return isset($this->link[$key]);
    }

    /**
     * Retrieve all provided links.
     *
     * @return array<string, string>
     */
    public function getLinks(): array
    {
        return $this->link;
    }

    /**
     * Retrieve a link url by the given case-insensitive name.
     *
     * @param[in] string $Name Link name
     * @param[in] string $Default Link default if not exists
     *
     * @return string
     */
    public function getLink(string $Name, string $Default = ""): string
    {
        $key = strtolower(trim($Name));

        return ($this->link[$key] ?? $Default);
    }

    /**
     * Return an instance with the specified link.
     *
     * @param[in] string $Name Link name
     * @param[in] string $Url Link valid url
     *
     * @return static
     */
    public function withLink(string $Name, string $Url): self
    {
        $key = strtolower(trim($Name));

        if (($Url != "") && filter_var($Url, FILTER_VALIDATE_URL)) {
            $this->link[$key] = $Url;
        } else {
            $this->withoutLink($Name);
        }

        return $this;
    }

    /**
     * Return an instance without the specified link.
     *
     * @param[in] string $Name Link name
     *
     * @return static
     */
    public function withoutLink(string $Name): self
    {
        $key = strtolower(trim($Name));

        if (isset($this->link[$key])) {
            unset($this->link[$key]);
        }

        return $this;
    }

    /**
     * Return an instance without any link.
     *
     * @return static
     */
    public function cleanLinks(): self
    {
        $this->link = array();

        return $this;
    }


    /**
     * Overwrite Psr7 withBody.
     *
     * @param[in] mixed $StreamInterface Body.
     *
     * @return static
     */
    public function withBody($StreamInterface): self
    {
        $log = new Syslog();
        $StreamInterface->rewind();
        $content = $StreamInterface->getContents();
        $StreamInterface->rewind();

        $json = @json_decode($content, true);

        if (is_array($json)) {
            $this->withSuccess(isset($json["success"]) &&  is_bool($json["success"]) ? $json["success"] : false);
            $this->withData(isset($json["data"]) && is_string($json["data"]) ? $json["data"] : null);

            $this->cleanMessages();
            foreach ((isset($json["message"]) && is_array($json["message"]) ? $json["message"] : array()) as $message) {
                if (is_array($message)) {
                    $this->addMessage(
                        ($message["type"] ?? ""),
                        ($message["text"] ?? ""),
                        ($message["data"] ?? null)
                    );
                }
            }

            $this->cleanLinks();
            foreach ((isset($json["link"]) && is_array($json["link"]) ? $json["link"] : array()) as $name => $url) {
                if (is_string($name) && is_string($url)) {
                    $this->withLink($name, $url);
                }
            }


            $this->parseErrorResponse($json);
        } else {
            $log->backtrace(json_last_error_msg());
        }

        return $this;
    }

    /**
     * Overwrite Message body.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody()
    {
        $json = array("success" => $this->success);

        if ($json["success"]) {
            $json["data"] = $this->data;
        } else {
            $this->raiseErrorResponse($json);
        }

        if (count($this->message) > 0) {
            $json["message"] = $this->message;
        }

        if (count($this->link) > 0) {
            $json["link"] = $this->link;
        }

        $content = json_encode($json);


        $stream = new Stream();

        $stream->withTemp();

        if (is_string($content)) {
            $this
                ->withHeader("Content-Type", "application/json")
                ->withHeader("Content-Length", strlen($content));

            $stream->write($content);
            $stream->rewind();
        }

        return $stream;
    }
}
