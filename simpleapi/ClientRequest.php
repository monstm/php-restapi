<?php

namespace Samy\SimpleApi;

use Psr\Http\Message\ResponseInterface;
use Samy\Psr18\Client;
use Samy\Psr7\Request;
use Samy\Psr7\Stream;
use Samy\Psr7\Uri;

/**
 * Simple SimpleApi ClientRequest implementation.
 */
class ClientRequest extends Request implements RequestInterface
{
    use RequestTrait;


    /**
     * Send request action.
     *
     * @param[in] string $Method Http method
     * @param[in] string $EndPoint Request end point
     * @param[in] array $Data Request data
     *
     * @return Response
     */
    public function sendRequest(string $Method, string $EndPoint, array $Data = array()): Response
    {
        $this->renderRequest($Method, $EndPoint, $Data);

        $client = new Client();
        $response = $client->sendRequest($this);

        return $this->renderResponse($response);
    }


    /**
     * Render request.
     *
     * @param[in] string $Method Http method
     * @param[in] string $EndPoint Request end point
     * @param[in] array $Data Request data
     *
     * @return void
     */
    private function renderRequest(string $Method, string $EndPoint, array $Data): void
    {
        $json = json_encode($Data);
        $content = (is_string($json) ? $json : "{}");


        $stream = new Stream();

        $stream->withTemp();
        $stream->write($content);
        $stream->rewind();


        $uri = new Uri();

        $uri->parseUrl($EndPoint);


        if ($this->hasAuthorization()) {
            $this->withHeader("Authorization", $this->getAuthorization());
        } else {
            $this->withoutHeader("Authorization");
        }

        $this
            ->withMethod($Method)
            ->withHeader("Accept", "application/json")
            ->withHeader("Content-Type", "application/json")
            ->withHeader("Content-Length", strlen($content))
            ->withBody($stream)
            ->withUri($uri);
    }

    /**
     * Render response.
     *
     * @param[in] ResponseInterface $Response SimpleApi response.
     *
     * @return Response
     */
    private function renderResponse(ResponseInterface $Response): Response
    {
        $ret = new Response();

        return $ret->parse($Response);
    }
}
