<?php

namespace Samy\SimpleApi;

/**
 * Describes SimpleApi Request interface.
 */
interface RequestInterface
{
    /**
     * Checks if a has authorization header.
     *
     * @return bool
     */
    public function hasAuthorization(): bool;

    /**
     * Return an instance with provided authorization header.
     *
     * @return string
     */
    public function getAuthorization(): string;


    /**
     * Retrieve provided authorization type.
     *
     * @return string
     */
    public function getAuthType(): string;

    /**
     * Return an instance with provided authorization type.
     *
     * @param[in] string $AuthType Authorization type
     *
     * @return static
     */
    public function withAuthType(string $AuthType): self;


    /**
     * Retrieve provided authorization credentials.
     *
     * @return string
     */
    public function getCredentials(): string;

    /**
     * Return an instance with provided authorization credentials.
     *
     * @param[in] string $Credentials Authorization credentials
     *
     * @return static
     */
    public function withCredentials(string $Credentials): self;
}
