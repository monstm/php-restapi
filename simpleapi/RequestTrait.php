<?php

namespace Samy\SimpleApi;

use Samy\RestApi\AuthType;

/**
 * Describes SimpleApi Request reuse code.
 */
trait RequestTrait
{
    /** describe authorization type */
    protected $auth_type = AuthType::BEARER;

    /** describe credentials */
    protected $credentials = "";


    /**
     * Checks if a has authorization header.
     *
     * @return bool
     */
    public function hasAuthorization(): bool
    {
        return ($this->auth_type != "") && ($this->credentials != "");
    }

    /**
     * Return an instance with provided authorization header.
     *
     * @return string
     */
    public function getAuthorization(): string
    {
        return $this->auth_type . " " . $this->credentials;
    }


    /**
     * Retrieve provided authorization type.
     *
     * @return string
     */
    public function getAuthType(): string
    {
        return $this->auth_type;
    }

    /**
     * Return an instance with provided authorization type.
     *
     * @param[in] string $AuthType Authorization type
     *
     * @return static
     */
    public function withAuthType(string $AuthType): self
    {
        $this->auth_type = $AuthType;

        return $this;
    }


    /**
     * Retrieve provided authorization credentials.
     *
     * @return string
     */
    public function getCredentials(): string
    {
        return $this->credentials;
    }

    /**
     * Return an instance with provided authorization credentials.
     *
     * @param[in] string $Credentials Authorization credentials
     *
     * @return static
     */
    public function withCredentials(string $Credentials): self
    {
        $this->credentials = $Credentials;

        return $this;
    }
}
