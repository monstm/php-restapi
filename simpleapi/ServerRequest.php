<?php

namespace Samy\SimpleApi;

use Samy\Psr7\ServerRequest as Psr7ServerRequest;

/**
 * Simple SimpleApi ServerRequest implementation.
 */
class ServerRequest extends Psr7ServerRequest implements RequestInterface
{
    use RequestTrait;

    /**
     * SimpleApi ServerRequest construction.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


        $buffer = $this->getAttribute("auth_type", "");
        if (is_string($buffer)) {
            $this->withAuthType($buffer);
        }

        $buffer = $this->getAttribute("auth_credentials", "");
        if (is_string($buffer)) {
            $this->withCredentials($buffer);
        }
    }
}
