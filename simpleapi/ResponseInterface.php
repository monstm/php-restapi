<?php

namespace Samy\SimpleApi;

/**
 * Describes SimpleApi Response interface.
 */
interface ResponseInterface
{
    /**
     * Retrieve provided success status.
     *
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * Return an instance with provided success status.
     *
     * @param[in] bool $Success Success status
     *
     * @return static
     */
    public function withSuccess(bool $Success): self;


    /**
     * Retrieve provided data.
     *
     * @return mixed
     */
    public function getData(): mixed;

    /**
     * Return an instance with provided data.
     *
     * @param[in] mixed $Data Data
     *
     * @return static
     */
    public function withData(mixed $Data): self;


    /**
     * Retrieve all provided messages.
     *
     * @return array<array<string, mixed>>
     */
    public function getMessages(): array;

    /**
     * Return an instance with added message.
     *
     * @param[in] string $Type Message type
     * @param[in] string $Text Message text
     * @param[in] mixed $Data Message data
     *
     * @return static
     */
    public function addMessage(string $Type, string $Text, mixed $Data = null): self;

    /**
     * Return an instance without any messages.
     *
     * @return static
     */
    public function cleanMessages(): self;


    /**
     * Checks if a link exists by the given case-insensitive name.
     *
     * @param[in] string $Name Link name
     *
     * @return bool
     */
    public function hasLink(string $Name): bool;

    /**
     * Retrieve all provided links.
     *
     * @return array<string, string>
     */
    public function getLinks(): array;

    /**
     * Retrieve a link url by the given case-insensitive name.
     *
     * @param[in] string $Name Link name
     * @param[in] string $Default Link default if not exists
     *
     * @return string
     */
    public function getLink(string $Name, string $Default = ""): string;

    /**
     * Return an instance with the specified link.
     *
     * @param[in] string $Name Link name
     * @param[in] string $Url Link valid url
     *
     * @return static
     */
    public function withLink(string $Name, string $Url): self;

    /**
     * Return an instance without the specified link.
     *
     * @param[in] string $Name Link name
     *
     * @return static
     */
    public function withoutLink(string $Name): self;

    /**
     * Return an instance without any link.
     *
     * @return static
     */
    public function cleanLinks(): self;


    /**
     * Retrieve provided error parameter
     *
     * @return string
     */
    public function getError(): string;

    /**
     * Return an instance with error parameter.
     *
     * @param[in] string $Error Error parameter.
     *
     * @return static
     */
    public function withError(string $Error): self;


    /**
     * Retrieve provided error description.
     *
     * @return string
     */
    public function getErrorDescription(): string;

    /**
     * Return an instance with error description.
     *
     * @param[in] string $ErrorDescription Error description.
     *
     * @return static
     */
    public function withErrorDescription(string $ErrorDescription): self;


    /**
     * Retrieve provided error uri.
     *
     * @return string
     */
    public function getErrorUri(): string;

    /**
     * Return an instance with error uri.
     *
     * @param[in] string $ErrorUri Error uri.
     *
     * @return static
     */
    public function withErrorUri(string $ErrorUri): self;
}
