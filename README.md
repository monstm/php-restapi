# PHP RestAPI

[
	![](https://badgen.net/packagist/v/samy/restapi/latest)
	![](https://badgen.net/packagist/license/samy/restapi)
	![](https://badgen.net/packagist/dt/samy/restapi)
	![](https://badgen.net/packagist/favers/samy/restapi)
](https://packagist.org/packages/samy/restapi)

REST is an acronym for REpresentational State Transfer and an architectural style for distributed hypermedia systems.
Roy Fielding first presented it in 2000 in his famous [dissertation](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm).

Like other architectural styles, REST has its guiding principles and constraints.
These principles must be satisfied if a service interface needs to be referred to as RESTful.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/restapi
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-restapi>
* Documentations: <https://monstm.gitlab.io/php-restapi/>
* Annotation: <https://monstm.alwaysdata.net/php-restapi/>
* Issues: <https://gitlab.com/monstm/php-restapi/-/issues>
