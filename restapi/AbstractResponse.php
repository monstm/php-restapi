<?php

namespace Samy\RestApi;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Samy\Log\Syslog;
use Samy\Psr7\Response;

/**
 * This is a simple Response implementation that other Response can inherit from.
 */
abstract class AbstractResponse extends Response
{
    /**
     * Return an instance with parsed PSR-7 response interface.
     *
     * @return static
     */
    public function parse(ResponseInterface $ResponseInterface): self
    {
        $log = new Syslog();

        try {
            foreach ($ResponseInterface->getHeaders() as $name => $value) {
                $this->withHeader($name, $value);
            }

            $this
                ->withProtocolVersion($ResponseInterface->getProtocolVersion())
                ->withBody($ResponseInterface->getBody())
                ->withStatus($ResponseInterface->getStatusCode(), $ResponseInterface->getReasonPhrase());
        } catch (Exception $exception) {
            $log = $log->exception($exception);
        }

        return $this;
    }
}
