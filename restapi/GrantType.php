<?php

namespace Samy\RestApi;

/**
 * Describes oauth2 grant type.
 *
 * @see https://datatracker.ietf.org/doc/html/rfc6749#page-73
 */
class GrantType
{
    public const CLIENT_CREDENTIALS = "client_credentials";
    public const PASSWORD           = "password";
    public const AUTHORIZATION_CODE = "authorization_code";
    public const REFRESH_TOKEN      = "refresh_token";
}
