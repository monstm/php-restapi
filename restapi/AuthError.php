<?php

namespace Samy\RestApi;

/**
 * Describes oauth2 error.
 *
 * @see https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/#error
 */
class AuthError
{
    public const INVALID_REQUEST        = "invalid_request";
    public const INVALID_CLIENT         = "invalid_client";
    public const INVALID_GRANT          = "invalid_grant";
    public const INVALID_SCOPE          = "invalid_scope";
    public const UNAUTHORIZED_CLIENT    = "unauthorized_client";
    public const UNSUPPORTED_GRANT_TYPE = "unsupported_grant_type";

    public const INVALID_REQUEST_DESCRIPTION        = "missing parameter";
    public const INVALID_CLIENT_DESCRIPTION         = "authentication failed";
    public const INVALID_GRANT_DESCRIPTION          = "authorization is invalid or expired";
    public const INVALID_SCOPE_DESCRIPTION          = "invalid scope value";
    public const UNAUTHORIZED_CLIENT_DESCRIPTION    = "client is not authorized";
    public const UNSUPPORTED_GRANT_TYPE_DESCRIPTION = "the authorization server does not recognize";
}
