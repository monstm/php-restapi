<?php

namespace Samy\RestApi;

/**
 * Simple Util implementation.
 */
class Util
{
    /**
     * Generate random bearer token with RFC 6750 spec.
     *
     * @param[in] int $Length The token length
     *
     * @see https://datatracker.ietf.org/doc/html/rfc6750
     *
     * @return string
     */
    public static function generateToken(int $Length): string
    {
        $ret = "";

        $character = "abcdefghijklmnopqrstuvwxyz" .
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" .
            "0123456789-._~+/";

        $limit = strlen($character) - 1;

        for ($pointer = 0; $pointer < $Length; $pointer++) {
            $ret .= $character[rand(0, $limit)];
        }

        return $ret;
    }

    /**
     * Generate random bearer token with RFC 6750 spec.
     *
     * @param[in] string $Datetime A valid datetime string formats
     *
     * @return int
     */
    public static function tokenExpire(string $Datetime): int
    {
        return time() - strtotime($Datetime);
    }
}
