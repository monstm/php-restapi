<?php

namespace Samy\RestApi;

/**
 * Describes HTTP status.
 *
 * @see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 */
class HttpStatus
{
    public const OK                     = 200;
    public const BAD_REQUEST            = 400;
    public const UNAUTHORIZED           = 401;

    public const INTERNAL_SERVER_ERROR  = 500;
}
