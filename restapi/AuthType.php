<?php

namespace Samy\RestApi;

/**
 * Describes HTTP Authentication type.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authentication_schemes
 */
class AuthType
{
    public const BASIC  = "Basic";
    public const BEARER = "Bearer";
    public const DIGIST = "Digest";
    public const HOBA   = "HOBA";
}
