<?php

namespace Samy\RestApi;

/**
 * Describes RestApi Error Response reuse code.
 *
 * @see https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/#error
 */
trait ErrorResponseTrait
{
    /** describe error parameter */
    protected $error = "";

    /** describe error_description */
    protected $error_description = "";

    /** describe error uri */
    protected $error_uri = "";


    /**
     * Retrieve provided error parameter
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * Return an instance with provided error parameter.
     *
     * @param[in] string $Error Error parameter.
     *
     * @return static
     */
    public function withError(string $Error): self
    {
        $this->error = $Error;

        return $this;
    }


    /**
     * Retrieve provided error description.
     *
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->error_description;
    }

    /**
     * Return an instance with provided error description.
     *
     * @param[in] string $ErrorDescription Error description.
     *
     * @return static
     */
    public function withErrorDescription(string $ErrorDescription): self
    {
        $this->error_description = $ErrorDescription;

        return $this;
    }


    /**
     * Retrieve provided error uri.
     *
     * @return string
     */
    public function getErrorUri(): string
    {
        return $this->error_uri;
    }

    /**
     * Return an instance with provided error uri.
     *
     * @param[in] string $ErrorUri Error uri.
     *
     * @return static
     */
    public function withErrorUri(string $ErrorUri): self
    {
        $this->error_uri = $ErrorUri;

        return $this;
    }


    /**
     * Return an instance with parsed error response.
     *
     * @param[in] array $Data Error response data.
     *
     * @return static
     */
    protected function parseErrorResponse(array $Data): self
    {
        $this->withError(
            isset($Data["error"]) &&
                is_string($Data["error"]) ?
                $Data["error"] : ""
        );

        $this->withErrorDescription(
            isset($Data["error_description"]) &&
                is_string($Data["error_description"]) ?
                $Data["error_description"] : ""
        );

        $this->withErrorUri(
            isset($Data["error_uri"]) &&
                is_string($Data["error_uri"]) ?
                $Data["error_uri"] : ""
        );

        return $this;
    }


    /**
     * Raise error response.
     *
     * @param[in,out] array $Result Error response data.
     *
     * @return static
     */
    protected function raiseErrorResponse(array &$Result): self
    {
        $buffer = $this->getError();
        if ($buffer != "") {
            $Result["error"] = $buffer;
        }

        $buffer = $this->getErrorDescription();
        if ($buffer != "") {
            $Result["error_description"] = $buffer;
        }

        $buffer = $this->getErrorUri();
        if ($buffer != "") {
            $Result["error_uri"] = $buffer;
        }

        return $this;
    }
}
