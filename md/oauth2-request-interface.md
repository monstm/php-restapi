# OAuth2 Request Interface

Describes OAuth2 Request interface.

---

## getGrantType

Retrieve provided grant type.

```php
$grant_type = $request->getGrantType();
```

---

## withGrantType

Return an instance with provided grant type.

```php
$request = $request->withGrantType($grant_type);
```

---

## getClientId

Retrieve provided client id.

```php
$client_id = $request->getClientId();
```

---

## withClientId

Return an instance with provided client id.

```php
$request = $request->withClientId($client_id);
```

---

## getClientSecret

Retrieve provided client secret.

```php
$client_secret = $request->getClientSecret();
```

---

## withClientSecret

Return an instance with provided client secret.

```php
$request = $request->withClientSecret($client_secret);
```

---

## getUsername

Retrieve provided username.

```php
$username = $request->getUsername();
```

---

## withUsername

Return an instance with provided username.

```php
$request = $request->withUsername($username);
```

---

## getPassword

Retrieve provided password.

```php
$password = $request->getPassword();
```

---

## withPassword

Return an instance with provided password.

```php
$request = $request->withPassword($password);
```

---

## getScope

Retrieve provided scope.

```php
$scope = $request->getScope();
```

---

## withScope

Return an instance with provided scope.

```php
$request = $request->withScope($scope);
```
