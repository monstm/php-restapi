# RestApi Constants

---

## AuthError

If the access token request is invalid,
such as the redirect URL didn't match the one used during authorization,
then the server needs to return an error response.

Please see [Unsuccessful Response](https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/#error) for more information.

| Name                                          | Value                                      |
|:----------------------------------------------|:-------------------------------------------|
| AuthError::INVALID_REQUEST                    | invalid_request                            |
| AuthError::INVALID_CLIENT                     | invalid_client                             |
| AuthError::INVALID_GRANT                      | invalid_grant                              |
| AuthError::INVALID_SCOPE                      | invalid_scope                              |
| AuthError::UNAUTHORIZED_CLIENT                | unauthorized_client                        |
| AuthError::UNSUPPORTED_GRANT_TYPE             | unsupported_grant_type                     |
| AuthError::INVALID_REQUEST_DESCRIPTION        | missing parameter                          |
| AuthError::INVALID_CLIENT_DESCRIPTION         | authentication failed                      |
| AuthError::INVALID_GRANT_DESCRIPTION          | authorization is invalid or expired        |
| AuthError::INVALID_SCOPE_DESCRIPTION          | invalid scope value                        |
| AuthError::UNAUTHORIZED_CLIENT_DESCRIPTION    | client is not authorized                   |
| AuthError::UNSUPPORTED_GRANT_TYPE_DESCRIPTION | the authorization server does not recogniz |

---

## AuthType

HTTP Authentication type.

Please see [Authentication schemes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authentication_schemes) for more information.

| Name             | Value  |
|:-----------------|:-------|
| AuthType::BASIC  | Basic  |
| AuthType::BEARER | Bearer |
| AuthType::DIGIST | Digest |
| AuthType::HOBA   | HOBA   |

---

## GrantType

Describes oauth2 grant type.

Please see ["grant_type" Syntax](https://datatracker.ietf.org/doc/html/rfc6749#page-73) for more information.

| Name                          | Value              |
|:------------------------------|:-------------------|
| GrantType::CLIENT_CREDENTIALS | client_credentials |
| GrantType::PASSWORD           | password           |
| GrantType::AUTHORIZATION_CODE | authorization_code |
| GrantType::REFRESH_TOKEN      | refresh_token      |

---

## HttpStatus

Describes HTTP status.

Please see [HTTP Status Codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) for more information.

| Name                              | Value |
|:----------------------------------|:-----:|
| HttpStatus::OK                    | 200   |
| HttpStatus::BAD_REQUEST           | 400   |
| HttpStatus::UNAUTHORIZED          | 401   |
| HttpStatus::INTERNAL_SERVER_ERROR | 500   |
