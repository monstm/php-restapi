# OAuth2 Request Instance

Simple OAuth2 Request Implementation.

---

## Server Request

Implementation OAuth2 ServerRequest instance.

```php
$request = new ServerRequest();
```

---

## Client Request

Implementation OAuth2 ClientRequest instance.

```php
$request = new ClientRequest();
```

### sendRequest

Send request action.

```php
$response = $request->sendRequest($endpoint);
```
