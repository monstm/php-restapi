# RestApi Utility

Simple Utility helper.

```php
$scope = new Scope();
```

---

## generateToken

Generate random bearer token with RFC 6750 spec.

Please see [RFC6750](https://datatracker.ietf.org/doc/html/rfc6750) for more information.

```php
$random_token = \Samy\RestApi\Util::generateToken($token_length);
```

---

## tokenExpire

Retrieve duration of token expire in seconds.

```php
$expires_in = \Samy\RestApi\Util::tokenExpire($datetime);
```
