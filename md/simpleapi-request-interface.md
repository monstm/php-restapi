# SimpleApi Request Interface

Describes SimpleApi Request interface.

---

## hasAuthorization

Checks if a has authorization header.

```php
$grant_type = $request->hasAuthorization();
```

---

## getAuthorization

Return an instance with provided authorization header.

```php
$authorization = $request->getAuthorization();
```

---

## getAuthType

Retrieve provided authorization type.

```php
$auth_type = $request->getAuthType();
```

---

## withAuthType

Return an instance with provided authorization type.

```php
$request = $request->withAuthType($auth_type);
```

---

## getCredentials

Retrieve provided authorization credentials.

```php
$credentials = $request->getCredentials();
```

---

## withCredentials

Return an instance with provided authorization credentials.

```php
$request = $request->withCredentials($credentials);
```
