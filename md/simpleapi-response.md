# SimpleApi Response

Implementation SimpleApi Response instance.

```php
$response = new Response();
```

---

## isSuccess

Retrieve provided success status.

```php
$is_success = $response->isSuccess();
```

---

## withSuccess

Return an instance with provided success status.

```php
$response = $response->withSuccess($is_success);
```

---

## getData

Retrieve provided data.

```php
$data = $response->getData();
```

---

## withData

Return an instance with provided data.

```php
$response = $response->withData($data);
```

---

## getMessages

Retrieve all provided messages.

```php
$messages = $response->getMessages();
```

---

## addMessage

Return an instance with added message.

```php
$response = $response->addMessage($type, $text, $data = null);
```

---

## cleanMessages

Return an instance without any messages.

```php
$response = $response->cleanMessages();
```

---

## hasLink

Checks if a link exists by the given case-insensitive name.

```php
$has_link = $response->hasLink($name);
```

---

## getLinks

Retrieve all provided links.

```php
$links = $response->getLinks();
```

---

## getLink

Retrieve a link url by the given case-insensitive name.

```php
$link = $response->getLink($name, $default = "");
```

---

## withLink

Return an instance with the specified link.

```php
$response = $response->withLink($name, $url);
```

---

## withoutLink

Return an instance without the specified link.

```php
$response = $response->withoutLink($name);
```

---

## cleanLinks

Return an instance without any link.

```php
$response = $response->cleanLinks();
```

---

## getError

Retrieve provided error parameter.

```php
$error = $response->getError();
```

---

## withError

Return an instance with provided error parameter.

```php
$response = $response->withError($error);
```

---

## getErrorDescription

Retrieve provided error description.

```php
$error_description = $response->getErrorDescription();
```

---

## withErrorDescription

Return an instance with provided error description.

```php
$response = $response->withErrorDescription($error_description);
```

---

## getErrorUri

Retrieve provided error uri.

```php
$error_uri = $response->getErrorUri();
```

---

## withErrorUri

Return an instance with provided error uri.

```php
$response = $response->withErrorUri($error_uri);
```

---

## parse

Return an instance with parsed PSR-7 response interface.

```php
$response = $response->parse($psr7_response_interface);
```
