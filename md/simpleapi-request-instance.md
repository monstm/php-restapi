# SimpleApi Request Instance

Simple SimpleApi Request Implementation.

---

## Server Request

Implementation SimpleApi ServerRequest instance.

```php
$request = new ServerRequest();
```

---

## Client Request

Implementation SimpleApi ClientRequest instance.

```php
$request = new ClientRequest();
```

### sendRequest

Send request action.

```php
$response = $request->sendRequest($method, $endpoint, $data);
```
