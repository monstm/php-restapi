# OAuth2 Response

Implementation OAuth2 Response instance.

```php
$response = new Response();
```

Please see [The Authorization Response](https://www.oauth.com/oauth2-servers/authorization/the-authorization-response/) for more information.

---

## getTokenType

Retrieve provided token type.

```php
$token_type = $response->getTokenType();
```

---

## withTokenType

Return an instance with provided token type.

```php
$response = $response->withTokenType($token_type);
```

---

## getAccessToken

Retrieve provided access token.

```php
$access_token = $response->getAccessToken();
```

---

## withAccessToken

Return an instance with provided access token.

```php
$response = $response->withAccessToken($access_token);
```

---

## getRefreshToken

Retrieve provided refresh token.

```php
$refresh_token = $response->getRefreshToken();
```

---

## withRefreshToken

Return an instance with provided refresh token.

```php
$response = $response->withRefreshToken($refresh_token);
```

---

## getExpiresIn

Retrieve provided token duration.

```php
$expires_in = $response->getExpiresIn();
```

---

## withExpiresIn

Return an instance with provided token duration.

```php
$response = $response->withExpiresIn($expires_in);
```

---

## getScope

Retrieve provided scope.

```php
$scope = $response->getScope();
```

---

## withScope

Return an instance with provided scope.

```php
$response = $response->withScope($scope);
```

---

## getError

Retrieve provided error parameter.

```php
$error = $response->getError();
```

---

## withError

Return an instance with provided error parameter.

```php
$response = $response->withError($error);
```

---

## getErrorDescription

Retrieve provided error description.

```php
$error_description = $response->getErrorDescription();
```

---

## withErrorDescription

Return an instance with provided error description.

```php
$response = $response->withErrorDescription($error_description);
```

---

## getErrorUri

Retrieve provided error uri.

```php
$error_uri = $response->getErrorUri();
```

---

## withErrorUri

Return an instance with provided error uri.

```php
$response = $response->withErrorUri($error_uri);
```

---

## parse

Return an instance with parsed PSR-7 response interface.

```php
$response = $response->parse($psr7_response_interface);
```
