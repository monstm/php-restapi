# RestApi Scope

The authorization and token endpoints allow the client to specify the scope of the access request using the "scope" request parameter.
In turn, the authorization server uses the "scope" response parameter to inform the client of the scope of the access token issued.

The value of the scope parameter is expressed as a list of space-delimited, case-sensitive strings.
The strings are defined by the authorization server.
If the value contains multiple space-delimited strings, their order does not matter,
and each string adds an additional access range to the requested scope.

```php
$scope = new Scope();
```

Please see [Access Token Scope](https://datatracker.ietf.org/doc/html/rfc6749#section-3.3) for more information.

---

## getValue

Retrieve a space-delimited string of the values.

```php
$value = $scope->getValue();
```

---

## getValues

Retrieve all the values.

```php
$values = $scope->getValues();
```

---

## hasValue

Checks if the given case-sensitive value is exists.

```php
$has_value = $scope->hasValue($value);
```

---

## withValue

Return an instance with the provided value.

```php
$scope = $scope->withValue($value);
```

---

## withAddedValue

Return an instance with the appended given value.

```php
$scope = $scope->withAddedValue($value);
```

---

## withoutValue

Return an instance without the specified value.

```php
$scope = $scope->withoutValue($value);
```
